package main

import (
	"fmt"
	"log"

	gitlab "github.com/xanzy/go-gitlab"
)

func main() {

	git, err := gitlab.NewClient("glpat-bhyCHKUZLxKcDqxa5rUZ")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	projectID := 43775850
	mergeRequests, _, err := git.MergeRequests.CreateMergeRequest(projectID, &gitlab.CreateMergeRequestOptions{
		SourceBranch: gitlab.String("staging"),
		TargetBranch: gitlab.String("main"),
		Title:        gitlab.String("Test MR from Go"),
	})
	if err != nil {
		log.Fatalf("Failed to create MR in %v with %v", projectID, err)
	}

	fmt.Println(mergeRequests.Title)

}
